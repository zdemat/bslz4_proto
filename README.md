# bslz4_proto

*Prototyping bitshuffle and lz4 decompression for FPGA.*

Note:
- The OpenCL code is expected to be finally implemented on FPGAs and is not optimised for other architectures and may be very ineffective in particular on CPUs.
- The code in this project is focused on functional testing and allows developement without any (heavy) FPGA OpenCL SDK.

### Compiling for emulator
```
aoc -march=emulator -legacy-emulator lz4.cl -DDEBUG_VERBOSE -o build/em/lz4.aocx -board=p520_hpc_m210h_g3x16
```

### Emulation
```
CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1 PYOPENCL_CTX='1' python lz4_app.py -d -c fpga -t -0
```

### Full FPGA flow
```
nohup aoc -D_USE_HBM_MEM_ lz4.cl -o build/p520_hpc_m210h_g3x16/lz4_vX.aocx -board=p520_hpc_m210h_g3x16 -v -report > nohup.out 2>&1 &
```

### FPGA test
```
PYOPENCL_CTX='0' python lz4_app.py -b p520_hpc_m210h_g3x16 -c fpga -v _vX -t 0
```
