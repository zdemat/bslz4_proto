//
// Performs LZ77 decoding for more than 1 element at once.
// Streams in 'LZ77InputData' (see common.hpp) appended with a flag (FlagBundle)
// which contains either a literal from upstream, or a {length, distance} pair.
// Given a literal input, this function simply tracks that literal in a history
// buffer and writes it to the output. For a {length, distance} pair, this
// function reads 'literals_per_cycle' elements from the history buffer per
// cycle and writes them to the output.
//
//  Template parameters:
//    InPipe: a SYCL pipe that streams in LZ77InputData with a boolean flag that
//      indicates whether the input stream is done.
//    OutPipe: a SYCL pipe that streams out an array of literals and a
//      'valid_count' that is in the range [0, literals_per_cycle].
//    literals_per_cycle: the number of literals to read from the history
//      buffer at once. This sets the maximum possible throughput for the
//      LZ77 decoder.
//    max_distance: the maximum distancefor a {length, distance} pair
//      For example, for DEFLATE this is 32K and for snappy this is 64K.
//

/* enable OpenCL extensions */
#pragma OPENCL EXTENSION cl_intel_channels : enable

// --- DEBUG MACROS ----------------------------------------------------------------------------------------------------

#ifdef DEBUG_VERBOSE
  #define _PRINTF(...)  printf(__VA_ARGS__)
#else
  #define _PRINTF(...)
#endif

// --- MEM FAGS --------------------------------------------------------------------------------------------------------

/* Memory models for different board configurations */
#ifdef _USE_HBM_MEM_ /* using multiple HBM memories */
    #define ATTRIBUTE_MEN_BANK0 __attribute((buffer_location("HBM0")))
    #define ATTRIBUTE_MEN_BANK1 __attribute((buffer_location("HBM1")))
#else                /* BLANK is default */
    #define ATTRIBUTE_MEN_BANK0
    #define ATTRIBUTE_MEN_BANK1
#endif

// --- lz77_decoder ----------------------------------------------------------------------------------------------------

// number of bits to count from 0 to literals_per_cycle-1
#define LZ77_history_buffer_buffer_idx_bits   2    /* log2(LZ77_literals_per_cycle) */
#define LZ77_literals_per_cycle  (1 << LZ77_history_buffer_buffer_idx_bits)
// exponent to get max_distance as a power of 2
#define LZ77_max_distance_exponent   16      /* log2(LZ77_max_distance) */
#define LZ77_max_distance        (1 << LZ77_max_distance_exponent)
// we will cyclically partition the history to 'literals_per_cycle' buffers,
// so each buffer gets this many elements
#define LZ77_history_buffer_count (LZ77_max_distance/LZ77_literals_per_cycle)
// bit mask for counting from 0 to literals_per_cycle-1
#define LZ77_history_buffer_buffer_idx_mask   (LZ77_literals_per_cycle - 1)
// number of bits to count from 0 to history_buffer_count-1
#define LZ77_history_buffer_idx_bits  (LZ77_max_distance_exponent - LZ77_history_buffer_buffer_idx_bits)  /* Log2(history_buffer_count) */
// bit mask for counting from 0 to history_buffer_count-1
#define LZ77_history_buffer_idx_mask  (LZ77_history_buffer_count - 1)

// TODO:
#define LZ77_max_literals LZ77_literals_per_cycle    /* max_literals <= literals_per_cycle */

// the data type used to index from 0 to literals_per_cycle-1 (i.e., pick
// which buffer to use)
#define LZ77_HistBufBufIdxT  uchar  /* uint(LZ77_history_buffer_buffer_idx_bits) */

// the data type used to index from 0 to history_buffer_count-1 (i.e., after
// picking which buffer, index into that buffer)
#define LZ77_HistBufIdxT   ushort /* uint(LZ77_history_buffer_idx_bits) */

// cache depth for on-chip history buffer
#define LZ77_kCacheDepth   8
 
typedef struct __attribute__ ((packed)) LZ77_InDataT
{
  unsigned char data_literal[LZ77_max_literals];
  unsigned short data_distance;
  unsigned short data_length;
  unsigned char data_valid_count;
  bool data_is_literal;
  bool flag;
} LZ77_InDataT;

typedef struct __attribute__ ((packed)) LZ77_OutDataT
{
  unsigned char data[LZ77_literals_per_cycle];
  unsigned char data_valid_count;
  bool flag;
} LZ77_OutDataT;

// --- end: lz77_decoder ------------------------------------------------------------------------------------------------

// --- byte_stacker -----------------------------------------------------------------------------------------------------

#define BSTACKER_literals_per_cycle    LZ77_literals_per_cycle 
#define BSTACKER_InDataT     LZ77_OutDataT
#define BSTACKER_OutDataT    BSTACKER_InDataT

// --- end: byte_stacker ------------------------------------------------------------------------------------------------

// --- lz4_reader -------------------------------------------------------------------------------------------------------

#define LZ4_bytes_per_cycle   LZ77_literals_per_cycle
#if LZ4_bytes_per_cycle < 5
  #define LZ4_kMaxReadBytes    5
#else
  #define LZ4_kMaxReadBytes    LZ4_bytes_per_cycle
#endif
#define LZ4_byte_stream_size  (2*LZ4_kMaxReadBytes)

#if LZ77_literals_per_cycle < LZ4_bytes_per_cycle
  #define LZ4_literals_per_cycle LZ77_literals_per_cycle
#else
  #define LZ4_literals_per_cycle LZ4_bytes_per_cycle
#endif

// LZ4_InDataT
typedef struct __attribute__ ((packed)) LZ4_InDataT
{
  unsigned char data[LZ4_bytes_per_cycle];
  unsigned char data_valid_count;
  bool flag;
} LZ4_InDataT;


channel LZ4_InDataT  ch_lz4_in;
channel LZ77_InDataT ch_lz4_out;

#define READING_NONE 0
#define READING_TOKEN 1
#define READING_LITERALS_LEN_BYTES 2
#define READING_LITERALS 3
#define READING_OFFSET 4
#define READING_MATCH_LEN_BYTES 5

// --- _get_valid_literals ------------------------------------------------------------------------------------------------
void _get_literals_valid_count(uchar* nvalid_literals, ushort nliterals_to_read, uchar byte_stream_cache_idx)
{
  if((byte_stream_cache_idx<=LZ4_literals_per_cycle) && (byte_stream_cache_idx<=nliterals_to_read))
    *nvalid_literals = byte_stream_cache_idx;
  //*nvalid_literals = ((byte_stream_cache_idx<=LZ4_literals_per_cycle) && (byte_stream_cache_idx<=nliterals_to_read)) ? byte_stream_cache_idx : 0;
  if((LZ4_literals_per_cycle<=nliterals_to_read) && (LZ4_literals_per_cycle<byte_stream_cache_idx))
    *nvalid_literals = LZ4_bytes_per_cycle;
  if((nliterals_to_read<byte_stream_cache_idx) && (nliterals_to_read<LZ4_literals_per_cycle))
    *nvalid_literals = nliterals_to_read;
}

// --- _reading_token -------------------------------------------------------------------------------------------------------
void _reading_token(uchar *reading_next, LZ77_InDataT *out_data_next, bool *write, char *nbytes_consumed,
					          ushort *data_length, ushort *nliterals_to_read,
          					uchar byte_stream_cache[LZ4_byte_stream_size+LZ4_kMaxReadBytes],
                    uchar byte_stream_cache_idx,
                    unsigned loop_counter)
{
  if(byte_stream_cache_idx==0 || byte_stream_cache_idx==1 || byte_stream_cache_idx==2) {
    *reading_next = READING_TOKEN;
    *write = false;
    *nbytes_consumed = 0;
    return;
  }

  _PRINTF("(%03d) reading token, byte_stream_cache_idx=%d\n", loop_counter, byte_stream_cache_idx);
  // the shortest sequence length is 3
  // we can trivially process 'short' pure copy operation or '2 literals'
  uchar token = byte_stream_cache[0];
  uchar tok_literals_len = (token & 0xf0) >> 4;
  ushort tok_match_len = (token & 0x0f);

  // speculate this is a 'short' pure copy, i.e. literals_len==0, match_len<19
  LZ77_InDataT out_data_3_cp;
  ushort offset = ((ushort)byte_stream_cache[1]) | (((ushort)byte_stream_cache[2]) << 8);
  out_data_3_cp.data_distance = offset;
  out_data_3_cp.data_length = tok_match_len + 4;
  out_data_3_cp.data_valid_count = 0;
  out_data_3_cp.data_is_literal = false;
  out_data_3_cp.flag = false; // the last 5 bytes are always literal

  if((token & 0xf0)==0) {
    if((token & 0x0f) != 15) {
      *out_data_next = out_data_3_cp;
      *write = true;
      *reading_next = READING_TOKEN;
      *nbytes_consumed = 3;
    } else {
      *write = false;
      *nbytes_consumed = 3;
      out_data_next->data_distance = offset;
      *data_length = 15; // keep for the next step
      out_data_next->data_length = 19;
      out_data_next->data_valid_count = 0;
      out_data_next->data_is_literal = false;
      out_data_next->flag = false; // the last 5 bytes are always literal
      *reading_next = READING_MATCH_LEN_BYTES;
    }
  } else {
    *write = false;
    *reading_next = READING_TOKEN;
    *data_length = token & 0x0f; // keep for future steps
    out_data_next->data_length = (token & 0x0f) + 4;
    // consume 1 byte
    *nbytes_consumed = 1;
    // decide if we are reading literals or literals_len bytes or offset
    if((token & 0xf0)==0)
      *reading_next = READING_OFFSET;
    else if((token & 0xf0)==(15<<4)) {
      *reading_next = READING_LITERALS_LEN_BYTES;
      *nliterals_to_read = 15;
    } else {
      *reading_next = READING_LITERALS;
      *nliterals_to_read = (token & 0xf0) >> 4;
    }
  }

  _PRINTF("(%03d) reading token, bytes_consumed=%d, reading_literals_len_bytes:%d, reading_literals:%d, reading_token=%d, reading_match_len_bytes=%d\n",
         loop_counter, *nbytes_consumed, *reading_next==READING_LITERALS_LEN_BYTES, *reading_next==READING_LITERALS, *reading_next==READING_TOKEN, *reading_next==READING_MATCH_LEN_BYTES);
}					

// --- _reading_literals -------------------------------------------------------------------------------------------------------
void _reading_literals(uchar *reading_next, LZ77_InDataT *out_data_next, bool *write, char *nbytes_consumed,
                       ushort *nliterals_to_read,
          					   uchar byte_stream_cache[LZ4_byte_stream_size+LZ4_kMaxReadBytes],
                       bool byte_flags_cache[LZ4_byte_stream_size+LZ4_kMaxReadBytes],
                       uchar byte_stream_cache_idx,
                       unsigned loop_counter)
{
  if(byte_stream_cache_idx == 0) {
    *reading_next = READING_LITERALS;
    *write = false;
    *nbytes_consumed = 0;
    return;
  }

  _PRINTF("(%03d) reading literals, nliterals_to_read=%d, byte_stream_cache_idx=%d\n", loop_counter, *nliterals_to_read, byte_stream_cache_idx);
  uchar nliterals_valid_count;
  _get_literals_valid_count(&nliterals_valid_count, *nliterals_to_read, byte_stream_cache_idx);
  #pragma unroll
  for(uchar i=0; i<LZ4_literals_per_cycle; i++)
    out_data_next->data_literal[i] = byte_stream_cache[i];
  out_data_next->data_valid_count = nliterals_valid_count;
  out_data_next->data_is_literal = true;
  out_data_next->data_length = 0;
  out_data_next->data_distance = 0;
  if(nliterals_valid_count==*nliterals_to_read) { //TODO:: can be faster
    out_data_next->flag = byte_flags_cache[*nliterals_to_read-1];
    *reading_next = !byte_flags_cache[*nliterals_to_read-1] ? READING_OFFSET : READING_NONE;
  } else {
    *reading_next = READING_LITERALS;
    *nliterals_to_read -= nliterals_valid_count;
    out_data_next->flag = false;
  }
  *write = true;
  // the last 5 bytes are always literal
  *nbytes_consumed = nliterals_valid_count;
  _PRINTF("(%03d) reading literals, nbytes_consumed=%d\n", loop_counter, *nbytes_consumed);
}

// --- _reading_offset ---------------------------------------------------------------------------------------------------------
void _reading_offset(uchar *reading_next, LZ77_InDataT *out_data_next, bool *write, char *nbytes_consumed,
                     ushort data_length,
          					 uchar byte_stream_cache[LZ4_byte_stream_size+LZ4_kMaxReadBytes],
                     uchar byte_stream_cache_idx,
                     unsigned loop_counter)
{
  if(byte_stream_cache_idx == 0 || byte_stream_cache_idx == 1) {
    *reading_next = READING_OFFSET;
    *write = false;
    *nbytes_consumed = 0;
    return;
  }

  _PRINTF("(%03d) reading offset, byte_stream_cache_idx=%d\n", loop_counter, byte_stream_cache_idx);
  // read 2 bytes of (copy op) offset
  ushort offset = ((ushort)(byte_stream_cache[0])) | ((ushort)(byte_stream_cache[1]) << 8);
  out_data_next->data_distance = offset;
  out_data_next->data_length = data_length + 4;
  if ((data_length & 0x0f) != 15) {
    // not reading any more match_len bytes
    *write = offset != 0;
    out_data_next->data_valid_count = 0;
    out_data_next->data_is_literal = false;
    out_data_next->flag = false;
    *reading_next = READING_TOKEN;
    _PRINTF("(%03d) reading offset, offset=%d, match_len=%d (write=%d)\n", loop_counter, offset, out_data_next->data_length, *write);
  } else {
    // need to read more match_len bytes
    *reading_next = READING_MATCH_LEN_BYTES;
    *write = false;
    _PRINTF("(%03d) reading offset, offset=%d ... will be reading match_len bytes\n", loop_counter, offset);
  }
  *nbytes_consumed = 2;
}

// --- _reading_literals_len_bytes ---------------------------------------------------------------------------------------------
void _reading_literals_len_bytes(uchar *reading_next, bool *write, char *nbytes_consumed,
                                 ushort *nliterals_to_read,
          					             uchar byte_stream_cache[LZ4_byte_stream_size+LZ4_kMaxReadBytes],
                                 uchar byte_stream_cache_idx,
                                 unsigned loop_counter)
{
  if(byte_stream_cache_idx == 0) {
    *reading_next = READING_LITERALS_LEN_BYTES;
    *write = false;
    *nbytes_consumed = 0;
    return;
  }

  _PRINTF("(%03d) reading literals_len_bytes, byte_stream_cache_idx=%d\n", loop_counter, byte_stream_cache_idx);
  uchar bytes[LZ4_bytes_per_cycle];
  #pragma unroll
  for(uchar i=0; i<LZ4_bytes_per_cycle; i++)
    bytes[i] = byte_stream_cache[i];
  ushort bytes_len[LZ4_bytes_per_cycle];
  bool byte_flag[LZ4_bytes_per_cycle];
  #pragma unroll
  for(uchar i=0; i<LZ4_bytes_per_cycle; i++)
    bytes_len[i] = i * 255;
  #pragma unroll
  for(uchar i=0; i<LZ4_bytes_per_cycle; i++) {
    bytes_len[i] += bytes[i];
    byte_flag[i] = bytes[i] != 255;
  }
  *write = false;
  bool have_all_bytes = false;
  uchar bytes_read    = (byte_stream_cache_idx < LZ4_bytes_per_cycle) ?  byte_stream_cache_idx    :  LZ4_bytes_per_cycle;
  uchar bytes_read_m1 = (byte_stream_cache_idx < LZ4_bytes_per_cycle) ? (byte_stream_cache_idx-1) : (LZ4_bytes_per_cycle-1);
  #pragma unroll
  for(uchar i=0; i<LZ4_bytes_per_cycle; i++) {
    if( (i < byte_stream_cache_idx) && byte_flag[i] ) {
      // test byte_flags for lower bytes
      bool test_passed = true;
      #pragma unroll
      for(uchar j=0; j<i; j++) {
        if( byte_flag[j] )
          test_passed = false;
      }
      if(test_passed) {
        bytes_read = i + 1;
        bytes_read_m1 = i;
        have_all_bytes = true;
      }
    }
  }
  *nliterals_to_read += bytes_len[bytes_read_m1];
  *reading_next = (have_all_bytes) ? READING_LITERALS : READING_LITERALS_LEN_BYTES;
  *nbytes_consumed = bytes_read;
  _PRINTF("(%03d) reading literals_len_bytes, nliterals_to_read=%d\n", loop_counter, *nliterals_to_read);
}

// --- _reading_match_len_bytes ------------------------------------------------------------------------------------------------
void _reading_match_len_bytes(uchar *reading_next, LZ77_InDataT *out_data_next, bool *write, char *nbytes_consumed,
          					          uchar byte_stream_cache[LZ4_byte_stream_size+LZ4_kMaxReadBytes],
                              uchar byte_stream_cache_idx,
                              unsigned loop_counter)
{
  if(byte_stream_cache_idx == 0) {
    *reading_next = READING_MATCH_LEN_BYTES;
    *write = false;
    *nbytes_consumed = 0;
    return;
  }

  _PRINTF("(%03d) reading match_len_bytes, byte_stream_cache_idx=%d\n", loop_counter, byte_stream_cache_idx);
  uchar bytes[LZ4_bytes_per_cycle];
  #pragma unroll
  for(uchar i=0; i<LZ4_bytes_per_cycle; i++)
    bytes[i] = byte_stream_cache[i];
  ushort bytes_match_len[LZ4_bytes_per_cycle];
  bool byte_flag[LZ4_bytes_per_cycle];
  #pragma unroll
  for(uchar i=0; i<LZ4_bytes_per_cycle; i++)
    bytes_match_len[i] = out_data_next->data_length + i * 255;
  #pragma unroll
  for(uchar i=0; i<LZ4_bytes_per_cycle; i++) {
    bytes_match_len[i] += bytes[i];
    byte_flag[i] = bytes[i] != 255;
  }
  *write = false;
  uchar bytes_read = (byte_stream_cache_idx < LZ4_bytes_per_cycle) ? byte_stream_cache_idx : LZ4_bytes_per_cycle;
  #pragma unroll
  for(uchar i=0; i<LZ4_bytes_per_cycle; i++) {
    if( (i < byte_stream_cache_idx) && byte_flag[i] ) {
      // test byte_flags for lower bytes
      bool test_passed = true;
      #pragma unroll
      for(uchar j=0; j<i; j++) {
        if( byte_flag[j] )
          test_passed = false;
      }
      if(test_passed) {
        bytes_read = i + 1;
        *write = true;
      }
    }
  }
  out_data_next->data_length = bytes_match_len[bytes_read-1];
  out_data_next->data_is_literal = false;
  *reading_next = (*write) ? READING_TOKEN : READING_MATCH_LEN_BYTES;
  *nbytes_consumed = bytes_read;
  _PRINTF("(%03d) reading match_len_bytes, data_length=%d\n", loop_counter, out_data_next->data_length);
}
 
// --- (kernel) lz4_reader -----------------------------------------------------------------------------------------------------

__kernel void lz4_reader()
{
  bool done = false;
  //bool done_next = false;
  bool not_done_reading = true;

  uchar reading_next = READING_TOKEN;

  ushort nliterals_to_read = 0;
  ushort data_length;
  uchar nliterals_valid_count = 0;
  
  uchar byte_stream_cache[LZ4_byte_stream_size+LZ4_kMaxReadBytes], pipe_data_cache[LZ4_byte_stream_size];
  bool byte_flags_cache[LZ4_byte_stream_size+LZ4_kMaxReadBytes], pipe_flags_cache[LZ4_byte_stream_size];
  
  uchar byte_stream_cache_sz = 0;
  uchar byte_stream_cache_space = LZ4_byte_stream_size;

  LZ77_InDataT out_data_next;
  bool write = false;

  bool read_not_stall = true;
  LZ4_InDataT pipe_data;
  char nbytes_consumed = 0;
  char npipe_bytes = 0;

//#ifdef DEBUG_VERBOSE
  unsigned int _idata = 0;
  unsigned int _odata = 0;

  unsigned loop_counter = 0;
  unsigned total_bytes_consumed_counter = 0;
//#endif

  while(!done) {

    #ifdef DEBUG_VERBOSE
    _PRINTF("pipe:");
    for(uchar i=0; i<LZ4_kMaxReadBytes; i++) {
      char c = pipe_data_cache[i];
      _PRINTF(" %d/%c", c, ((c>='a' && c<='z') || (c>='A' && c<='Z') || (c>='0' && c<='9')) ? c : '@');
    }
    _PRINTF("\n");
    _PRINTF("(%03d) byte_stream_cache(sz=%d):", loop_counter, byte_stream_cache_sz);
    for(uchar i=0; i<byte_stream_cache_sz; i++) {
      char c = byte_stream_cache[i];
      _PRINTF(" %d/%c", c, ((c>='a' && c<='z') || (c>='A' && c<='Z') || (c>='0' && c<='9')) ? c : '@');
    }
    _PRINTF("\n");
    #endif

    if(write) {
      write_channel_intel(ch_lz4_out, out_data_next);
      write = false;
      #ifdef DEBUG_VERBOSE
      _PRINTF("(%03d) Writing: data_is_literal=%d, flag=%d, valid_count=%d, match_len=%d, offset=%d", 
              loop_counter, out_data_next.data_is_literal, out_data_next.flag, out_data_next.data_valid_count, out_data_next.data_length, out_data_next.data_distance);
      if(out_data_next.data_is_literal) {
        _PRINTF(":");
        for(uchar i=0; i<LZ77_literals_per_cycle; i++)
          _PRINTF(" %d/%c", out_data_next.data_literal[i], out_data_next.data_literal[i]);
      }
      _PRINTF("\n");
      _odata++;
      #endif
      done = out_data_next.flag;
    }

    uchar token_reading_next;
    LZ77_InDataT token_out_data_next;
    bool token_write;
    char token_nbytes_consumed;
		ushort token_data_length;
    ushort token_nliterals_to_read;
    
    #ifdef DEBUG_VERBOSE
    if(reading_next==READING_TOKEN)
    #endif
    _reading_token(&token_reading_next, &token_out_data_next, &token_write, &token_nbytes_consumed, &token_data_length,
                   &token_nliterals_to_read,
           				 byte_stream_cache, byte_stream_cache_sz, loop_counter);
    
    uchar literals_reading_next;
    LZ77_InDataT literals_out_data_next = out_data_next;
    bool literals_write;
    char literals_nbytes_consumed;
    ushort literals_nliterals_to_read = nliterals_to_read;

    #ifdef DEBUG_VERBOSE
    if(reading_next==READING_LITERALS)
    #endif
    _reading_literals(&literals_reading_next, &literals_out_data_next, &literals_write, &literals_nbytes_consumed,
                      &literals_nliterals_to_read,
       					      byte_stream_cache, byte_flags_cache, byte_stream_cache_sz, loop_counter);

    uchar offset_reading_next;
    LZ77_InDataT offset_out_data_next = out_data_next;
    bool offset_write;
    char offset_nbytes_consumed;
    
    #ifdef DEBUG_VERBOSE
    if(reading_next==READING_OFFSET)
    #endif
    _reading_offset(&offset_reading_next, &offset_out_data_next, &offset_write, &offset_nbytes_consumed,
                    data_length,
           				  byte_stream_cache, byte_stream_cache_sz, loop_counter);    

    uchar literals_len_bytes_reading_next;
    bool literals_len_bytes_write;
    char literals_len_bytes_nbytes_consumed;
    ushort literals_len_bytes_nliterals_to_read = nliterals_to_read;

    #ifdef DEBUG_VERBOSE
    if(reading_next==READING_LITERALS_LEN_BYTES)
    #endif
    _reading_literals_len_bytes(&literals_len_bytes_reading_next, &literals_len_bytes_write, &literals_len_bytes_nbytes_consumed,
                                &literals_len_bytes_nliterals_to_read,
          					            byte_stream_cache, byte_stream_cache_sz, loop_counter);

    uchar match_len_bytes_reading_next;
    LZ77_InDataT match_len_bytes_out_data_next = out_data_next;
    bool match_len_bytes_write;
    char match_len_bytes_nbytes_consumed;

    #ifdef DEBUG_VERBOSE
    if(reading_next==READING_MATCH_LEN_BYTES)
    #endif
    _reading_match_len_bytes(&match_len_bytes_reading_next, &match_len_bytes_out_data_next, &match_len_bytes_write,
                             &match_len_bytes_nbytes_consumed,
          					         byte_stream_cache, byte_stream_cache_sz, loop_counter);

    switch(reading_next) {
      case READING_TOKEN:
        reading_next = token_reading_next;
        out_data_next = token_out_data_next;
        write = token_write;
        nbytes_consumed = token_nbytes_consumed;
        data_length = token_data_length;
        nliterals_to_read = token_nliterals_to_read;
        break;
      case READING_LITERALS:
        reading_next = literals_reading_next;
        out_data_next = literals_out_data_next;
        write = literals_write;
        nbytes_consumed = literals_nbytes_consumed;
        nliterals_to_read = literals_nliterals_to_read;       
        break;
      case READING_OFFSET:
        reading_next = offset_reading_next;
        out_data_next = offset_out_data_next;
        write = offset_write;
        nbytes_consumed = offset_nbytes_consumed;
        break;
      case READING_LITERALS_LEN_BYTES:
        reading_next = literals_len_bytes_reading_next;
        write = literals_len_bytes_write;
        nbytes_consumed = literals_len_bytes_nbytes_consumed;
        nliterals_to_read = literals_len_bytes_nliterals_to_read;
        break;
      case READING_MATCH_LEN_BYTES:
        reading_next = match_len_bytes_reading_next;
        out_data_next = match_len_bytes_out_data_next;
        write = match_len_bytes_write;
        nbytes_consumed = match_len_bytes_nbytes_consumed;
        break;
      default:
        break;
    }
    
    // shift casche
    uchar _byte_stream_cache[LZ4_bytes_per_cycle+1][LZ4_byte_stream_size];
    bool _byte_flags_cache[LZ4_bytes_per_cycle+1][LZ4_byte_stream_size];

    _PRINTF("byte_stream_cache_sz=%d\n", byte_stream_cache_sz);
    #pragma unroll
    for(uchar ishift=0; ishift<=LZ4_bytes_per_cycle; ishift++) {
      #pragma unroll
      for(char i=0; i<LZ4_byte_stream_size; i++) {
        _byte_stream_cache[ishift][i] = ((i+ishift)<byte_stream_cache_sz) ? byte_stream_cache[i+ishift] : pipe_data_cache[i+ishift-byte_stream_cache_sz]; // alternative: speculated add
        _byte_flags_cache[ishift][i] = ((i+ishift)<byte_stream_cache_sz) ? byte_flags_cache[i+ishift] : pipe_flags_cache[i+ishift-byte_stream_cache_sz]; // alternative: speculated add        
        _PRINTF(" %d", _byte_stream_cache[ishift][i]);
      }
      _PRINTF("\n");
    }
    #pragma unroll
    for(uchar i=0; i<LZ4_byte_stream_size; i++) {
      byte_stream_cache[i] = _byte_stream_cache[nbytes_consumed][i];
      byte_flags_cache[i] = _byte_flags_cache[nbytes_consumed][i];      
    }

    byte_stream_cache_sz    += (char)(npipe_bytes - nbytes_consumed);
    byte_stream_cache_space -= (char)(npipe_bytes - nbytes_consumed);

    bool valid_read = false;
    if(read_not_stall) {
      // try to read in some new data
      _PRINTF("(%03d) reading data, _idata=%d\n", loop_counter, _idata);
      pipe_data = read_channel_nb_intel(ch_lz4_in, &valid_read);
      #ifdef DEBUG_VERBOSE
      if(valid_read)
        _idata++;
      #endif
    } // if(read_not_stall)
    npipe_bytes = (valid_read) ? pipe_data.data_valid_count : 0;
    #pragma unroll
    for(char i=0; i<LZ4_kMaxReadBytes; i++) {
      pipe_data_cache[i] = (valid_read) ? pipe_data.data[i] : pipe_data_cache[i];
      pipe_flags_cache[i] = (valid_read) ? (pipe_data.flag && ((i+1)==pipe_data.data_valid_count)) : pipe_flags_cache[i];
    }

    read_not_stall = byte_stream_cache_space >= 2*LZ4_bytes_per_cycle;

    #ifdef DEBUG_VERBOSE
    total_bytes_consumed_counter += nbytes_consumed;   
    loop_counter++;
    _PRINTF("(%03d) loop counter incremented, total bytes consumed=%d\n", loop_counter, total_bytes_consumed_counter);
    #endif

  } // while(!done)

} // __kernel void lz4_reader()

// --- end: lz4_reader --------------------------------------------------------------------------------------------------

/* Memory models for different board configurations */
#ifdef _USE_HBM_MEM_ /* using multiple HBM memories */
    #define ATTRIBUTE_MEN_BANK0 __attribute((buffer_location("HBM0")))
    #define ATTRIBUTE_MEN_BANK1 __attribute((buffer_location("HBM1")))
#else                /* BLANK is default */
    #define ATTRIBUTE_MEN_BANK0
    #define ATTRIBUTE_MEN_BANK1
#endif

__kernel void producer( global ATTRIBUTE_MEN_BANK0 const LZ4_InDataT* restrict idata,
                        const unsigned int ilen)
{ 
  bool flag = (ilen == 1);

  const uint ilenm2 = ilen - 2;

  const uint nmul = 1;

  for(uint i=0; i<nmul*ilen; i++) {
    if (i % nmul == 0) {
      LZ4_InDataT data = idata[i / nmul];
      //data.flag = flag;
      write_channel_intel(ch_lz4_in, data);      
    }
    flag = i == ilenm2;
    //mem_fence(CLK_CHANNEL_MEM_FENCE);
  }
}

__kernel void consumer( global ATTRIBUTE_MEN_BANK1 LZ77_InDataT* restrict odata,
                        const unsigned int olen_max)
{
  bool done = false;
  uint i = 0;
  while(!done) {
    LZ77_InDataT data = read_channel_intel(ch_lz4_out);
    done = data.flag;
    odata[i % olen_max] = data;
    i++;
  }
}

