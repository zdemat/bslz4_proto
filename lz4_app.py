#!/usr/bin/env python

# Refences:
# [1] https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-wusp/fb98aa28-5cd7-407f-8869-a6cef1ff1ccb
# [2] https://github.com/oneapi-src/oneAPI-samples/tree/master/DirectProgramming/C%2B%2BSYCL_FPGA/ReferenceDesigns/decompress

# lz4_app.py - prototyping single task lz4 "reader" for FPGAs applications
#            - no-channel cpu/pocl version

import numpy as np
import pyopencl as cl
import pyopencl.cltypes
import os, sys, getopt
import struct
import lz4.block

# -------------------------------------- parse arguments --------------------------------------
debug = False
cl_build = 'auto'
board_name = 'em'
aocx_version = ''
test_id = 0
LZ77_n_literals = 4
LZ4_n_bytes = LZ77_n_literals

ctx = None # to avoid multiple reprogramming
prg = None # to avoid multiple reprogramming
prg_init = True # to avoid multiple reprogramming
krn_lz4 = None # to avoid the error msg
krn_in, krn_out = None, None

try:
    opts, args = getopt.getopt(sys.argv[1:],"hdb:v:t:c:",["board=","version=","test=","clbuild=",])
except getopt.GetoptError:
    print('lz4_app.py -b [-d] <board-name> -v <version-aocx> -clbuild <cl-build-opt> -t <test-id>')
    sys.exit(2)
for opt, arg in opts:
    if opt in ('-h', '--help'):
        print('lz4_app.py [-d] -b <board-name> -v <version-aocx> -clbuild <cl-build-opt> -t <test-id>')
        sys.exit()
    if opt in ('-d', '--debug'):
        debug = True
    elif opt in ("-b", "--board"):
        board_name = arg
    elif opt in ("-v", "--version"):
        aocx_version = arg
    elif opt in ("-c", "--clbuild"):
        cl_build = arg
    elif opt in ("-t", "--test"):
        test_id = int(arg)

print('borad-name:', board_name)
print('version-aocx:', aocx_version)
print('cl-build:', cl_build)
print('debug:', debug)
print('test:', test_id)
# ---------------------------------------------------------------------------------------------

CL_SRC_PATH = './'
CL_SRC_NAME = 'lz4.cl'

AOCX_BIN_PATH = os.path.join('./build', board_name)
AOCX_BIN_NAME = 'lz4' + aocx_version + '.aocx'

def lz77_decode_inst_step(output, lz77_inst, verbose=False):
    if lz77_inst['is_literal'] > 0:
        # literals: only add literals
        output.extend(lz77_inst['literals'][:lz77_inst['valid_count']])
        decoded_output_bytes = lz77_inst['valid_count']
        if verbose:
            print('literals: %s' % bytearray(lz77_inst['literals'][:lz77_inst['valid_count']]).decode('utf-8'))
    else:
        initial_output_len = len(output)
        # match copy operation
        offset = lz77_inst['dist']
        match_len = lz77_inst['len']
        if verbose:
            print('match copy op: offset=%d, match_len=%d' % (offset, match_len,))
        if offset>0:
            i_offset = len(output) - offset
            for i in range(match_len):
                output.extend(output[i_offset:i_offset+1])
                i_offset += 1
        decoded_output_bytes = len(output) - initial_output_len
    return output, decoded_output_bytes, lz77_inst['flag']>0

def lz77_decode_instructions(lz77_inst_list, verbose=False):
    output = bytearray()
    out_bytes = 0
    for lz77_inst in lz77_inst_list:
        output, _out_bytes, flag = lz77_decode_inst_step(output=output, lz77_inst=lz77_inst, verbose=verbose)
        out_bytes += _out_bytes
    return output, out_bytes, flag
    
def LZ77_InDataT_pack(literal, dist, length, valid_count, is_literal, flag):
    return np.frombuffer(struct.pack('<'+'b'*LZ77_n_literals+'HHbbb', *literal, dist, length, valid_count, is_literal, flag), dtype=np.ubyte)

def LZ77_OutDataT_unpack(data):
    _t = struct.unpack('<'+'b'*LZ77_n_literals+'bb', data)
    return _t[:LZ77_n_literals], _t[LZ77_n_literals], _t[LZ77_n_literals+1]>0 # data, valid_count, flag

def LZ4_InDataT_pack(bytes_arr, offset=0, flag=False):
    if (offset + LZ4_n_bytes) > bytes_arr.size:
        pad = np.frombuffer(b'\0'*(offset + LZ4_n_bytes - bytes_arr.size), dtype=np.ubyte)
        _bytes = np.concatenate((bytes_arr[offset:], pad,))
        valid_count = bytes_arr.size - offset
    else:
        _bytes = bytes_arr[offset:offset+LZ4_n_bytes]
        valid_count = LZ4_n_bytes
    return np.frombuffer(struct.pack('<'+'B'*LZ4_n_bytes+'BB', *_bytes, valid_count, flag), dtype=np.ubyte)

def LZ4_bytes(literals='', dist=0, length=0):
    # calculate LZ4 sequence parameters
    nliterals_len_bytes = (len(literals)-15 + 255) // 255 if (len(literals)>=15) else 0
    nmatch_len_bytes = (length-19 + 255) // 255 if (length>=19) else 0
    literals_len_bytes = [255,]*(nliterals_len_bytes-1) + [(len(literals)-15) % 255] if (nliterals_len_bytes>0) else []
    match_len_bytes = [255,]*(nmatch_len_bytes-1) + [(length-19) % 255] if (nmatch_len_bytes>0) else []
    nliterals_len_bytes = (len(literals)-15 + 255) // 255 if (len(literals)>=15) else 0
    # create the LZ4 sequence
    token  = len(literals) << 4 if len(literals)<15 else 15 << 4
    token |= 15 if length>=19 else length-4 if length>4  else 0
    bytes_arr = np.frombuffer(  struct.pack('<B'+'B'*nliterals_len_bytes, token, *literals_len_bytes)
                              + literals.encode('utf-8')
                              + struct.pack('<H', dist)
                              + struct.pack('<'+'B'*nmatch_len_bytes, *match_len_bytes), dtype=np.ubyte)
    return bytes_arr

def LZ4_partition_bytes(bytes_arr):
    # partition the LZ4 sequence in a series of LZ4_InDataT
    ndata = (bytes_arr.size + LZ4_n_bytes - 1) // LZ4_n_bytes
    data = ()
    for i in range(ndata):
        d = LZ4_InDataT_pack(bytes_arr, offset=i*LZ4_n_bytes, flag=i==(ndata-1))
        data += (d,)
    return data

def LZ77_InDataT_unpack(bytes_arr, offset=0):
    # item size: LZ77_n_literals[B] + 2[B] + 2[B] + 1[B] + 1[b] + 1[b]
    item_sz = LZ77_n_literals + 7
    if((offset+item_sz)>bytes_arr.size):
        raise IndexError("LZ77 array out of range")
    r = {}
    r['literals'] = [b'\00']*LZ77_n_literals
    *r['literals'], r['dist'], r['len'], r['valid_count'], r['is_literal'], r['flag'] = struct.unpack('<'+'B'*LZ77_n_literals+'HHBBB', bytes_arr[offset:offset+item_sz])
    return r, item_sz

def LZ4_test0():
    b0 = LZ4_bytes(literals='xyzw', dist=0, length=0)
    b1 = LZ4_bytes(literals='ABCDEFGHIJKL', dist=2, length=4)
    print(b1)
    b2 = LZ4_bytes(literals='abcd', dist=0, length=0)
    b3 = LZ4_bytes(literals='', dist=4, length=4)
    b4 = LZ4_bytes(literals=' a long sequence to be repeated twice ', dist=37, length=36)
    print(b4)
    # the last 5 bytes are always literals
    bend = LZ4_bytes(literals=' ...this is the end', dist=0, length=0)
    bend = bend[:-2] # cut the offset
    bsrc = np.concatenate((b0,b1,b2,b3,b4, bend,))
    data = LZ4_partition_bytes(bsrc)
    return np.concatenate(data), len(data), np.frombuffer(b'xyzwABCDEFGHIJKLKLKLabcdabcd a long sequence to be repeated twice a long sequence to be repeated twice ...this is the end', np.uint8)

def LZ4_test1(block_nb=0):
    compressed = np.load('data/comressed_bslz4.npz')['arr_0']
    compressed_buf = compressed.data.cast('B')
    sz = struct.unpack('>q', compressed_buf[:8])[0]
    block_sz = struct.unpack('>i', compressed_buf[8:12])[0]
    offset_in = 12
    iblk = 0
    if compressed.size < 13:
        raise IndexError('block_nb too high')
    while True:
        compressed_block_sz = struct.unpack('>i',compressed_buf[offset_in:offset_in+4])[0]
        if iblk!=block_nb:
            offset_in_new = offset_in + compressed_block_sz + 4
            if ((offset_in_new+13-4)>compressed.size): # a file with less then 13 bytes can only be represented as literals
                raise IndexError('block_nb too high')
            offset_in = offset_in_new
            iblk += 1
        else:
            break
    compressed_block = compressed.astype(np.uint8)[offset_in+4:offset_in+4+compressed_block_sz]

    data = LZ4_partition_bytes(compressed_block)
    # a reference
    #uncompressed = np.load('shuffeled_frame_bs.npz')['arr_0']
    try:
        #print(offset_in, compressed_block[:4])
        uncompressed_block = np.frombuffer(lz4.block.decompress(source=compressed_block, uncompressed_size=block_sz), dtype=np.uint8)
    except Exception as e:
        print('(Error during lz4.block.decompress block_nb=%d)' % block_nb, e)
        print(offset_in, compressed_block[:4])
        raise

    return np.concatenate(data), len(data), uncompressed_block, compressed.size - (offset_in+4+compressed_block_sz)

def LZ4_test2():
    b0 = LZ4_bytes(literals='abcd', dist=0, length=0)
    b1 = LZ4_bytes(literals='ABCD', dist=0, length=0)
    b2 = LZ4_bytes(literals='wxyz', dist=0, length=0)
    b3 = LZ4_bytes(literals='', dist=4, length=4)
    # the last 5 bytes are always literals
    bend = LZ4_bytes(literals=' LAST', dist=0, length=0)
    bend = bend[:-2] # cut the offset
    bsrc = np.concatenate((b0,b1,b2,b3, bend,))
    data = LZ4_partition_bytes(bsrc)
    return np.concatenate(data), len(data), np.frombuffer(b'abcdABCDwxyzwxyz LAST', np.uint8)

def LZ4_test3():
    b0 = LZ4_bytes(literals='abcd', dist=0, length=0)
    b1 = LZ4_bytes(literals='ABC', dist=0, length=0)
    b2 = LZ4_bytes(literals='Dw', dist=0, length=0)
    b3 = LZ4_bytes(literals='xyz', dist=0, length=0)
    b4 = LZ4_bytes(literals='', dist=4, length=4)
    # the last 5 bytes are always literals
    bend = LZ4_bytes(literals=' LAST', dist=0, length=0)
    bend = bend[:-2] # cut the offset
    bsrc = np.concatenate((b0,b1,b2,b3,b4, bend,))
    data = LZ4_partition_bytes(bsrc)
    return np.concatenate(data), len(data), np.frombuffer(b'abcdABCDwxyzwxyz LAST', np.uint8)

# --- single OpenCL execution  ---
def lz4_reader_cl(data_in, ilen, olen=None, verbose=True):
    """Single OpenCL kernel run"""

    global ctx, prg, prg_init, krn_lz4, krn_in, krn_out
    
    # a helper function to create aligned numpy arrays    
    def np_aligned_empty(shape, dtype, alignbytes):
        # https://stackoverflow.com/questions/9895787/memory-alignment-for-fast-fft-in-python-using-shared-arrays
        dtype = np.dtype(dtype)
        nbytes = np.prod(shape) * dtype.itemsize
        buf = np.empty(nbytes+alignbytes, dtype=np.uint8)
        start_idx = -buf.ctypes.data % alignbytes
        return buf[start_idx:start_idx + nbytes].view(dtype).reshape(shape)

    # --- wrap input data into aligned arrays on host ---
    item_sz = LZ4_n_bytes + 2
    otem_sz = LZ77_n_literals + 2
    if olen in [None,[],-1]:
        olen = 1024**2

    # data type of control arrays must fit the kernel
    idata_h = np_aligned_empty((ilen*item_sz,), np.ubyte, 64); idata_h[()] = data_in
    odata_h = np_aligned_empty((olen*otem_sz,), np.ubyte, 64); odata_h[()] = 0

    # a standard opencl dance:
    # 1) create context
    # 2) load OpenCL image (source for GPU, binary for FPGA)
    # 3) create program
    # 4) get kernels
    # 5) create queue
    # 6) allocate space on device
    # 7) set kernel parameters
    # 7) copy input from the host memory to device
    # 8) enquque kernels
    # 9) copy result from device to host

    # --- create some context ---
    if ctx in [None,[],'']:
        ctx = cl.create_some_context()

    # --- load OpenCL source or binary ---

    if cl_build in [None,[],'auto']:
        _cl_build = 'src' if 'FPGA' not in ctx.devices[0].version else 'fpga'
        print('cl-build:', _cl_build)
    else:
        _cl_build = cl_build.lower()
    
    if _cl_build=='src':
        # load kernel source
        cl_src_filename = os.path.join(CL_SRC_PATH, CL_SRC_NAME)

        if verbose or prg_init:
            print("[azint-cl] OpenCL will use CL src: %s" % (cl_src_filename,))

        # load kernel source
        with open( cl_src_filename, 'r', encoding='utf-8') as fid:
            cl_src = ''.join(fid.readlines())
    else:
        # check emulation mode
        fpga_emulation = os.getenv('CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA')=='1'
        if(fpga_emulation):
            if verbose or prg_init:
                print("[azint-cl] FPGA emulation mode (ignore kernel programming error)")

        # load bitstream kernel
        aocx_filename = os.path.join(AOCX_BIN_PATH, AOCX_BIN_NAME)

        if verbose or prg_init:
            print("[azint-cl] OpenCL will use AOCX binary image: %s" % (aocx_filename,))

        # load bitstream kernel
        with open( aocx_filename, "rb") as fid:
            cl_binary = fid.read()

    # --- program, kernels and queue ---
    
    # OpenCL program
    if prg_init: # avoid multiple reprogramming
        if _cl_build=='src':
            prg = cl.Program(ctx, cl_src).build(options=['-DDEBUG','-DDEBUG_VERBOSE',] if debug else [])
        else:
            prg = cl.Program(ctx, ctx.devices, [cl_binary,])
        prg_init = False

    # compute kernel(s)
    if krn_lz4 in [None,[],'']:
        krn_in = prg.producer
        krn_lz4 = prg.lz4_reader
        krn_out = prg.consumer

    # create OpenCL event queue(s)
    queue_in  = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)
    queue_lz4 = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)
    queue_out = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)

    # --- allocate space on device ---
    
    mf = cl.mem_flags    

    # enqueue copy can be done only after setting the kernel parameters !
    # Note: do not use mf.ALLOC_HOST_PTR !
    idata_d = cl.Buffer(ctx, mf.READ_ONLY, idata_h.size*idata_h.dtype.itemsize)
    odata_d = cl.Buffer(ctx, mf.WRITE_ONLY, odata_h.size*odata_h.dtype.itemsize)

    # --- set kernel parameters ---
    # Note: This must be done before enqueue copy in order to enforce the altera memory flags
    krn_in.set_args(idata_d, np.uint32(ilen))
    krn_out.set_args(odata_d, np.uint32(olen))

    # --- copy input from the host memory to device ---
    cl.enqueue_copy(queue_in, idata_d, idata_h, is_blocking=True)
    #queue_in.finish()

    # --- enqueue kernels ---
    evt_out = cl.enqueue_nd_range_kernel(queue_out, krn_out, (1,1,1), (1,1,1)) # single task kernel
    evt_lz4 = cl.enqueue_nd_range_kernel(queue_lz4, krn_lz4, (1,1,1), (1,1,1)) # single task kernel
    evt_in  = cl.enqueue_nd_range_kernel(queue_in , krn_in , (1,1,1), (1,1,1)) # single task kernel
    queue_lz4.flush()
    queue_out.flush()
    queue_in.flush()
    cl.wait_for_events([evt_lz4, evt_in, evt_out])

    # --- copy result from device to host ---
    cl.enqueue_copy(queue_out, odata_h, odata_d, is_blocking=True)

    # print some statistics
    #t_ms = (evt_hst.profile.end-evt_hst.profile.start)*1e-6
    #print("[azint-cl] exec. time transpose(1): %.2f ms, %.2f MHz" % (t_ms,ilen*1e-3/t_ms,))

    # release events
    evt_lz4 = None
    evt_in = None
    evt_out = None

    # return result
    return odata_h

def lz4_reader_run(idata, ilen, verbose=True):
    # run lz4 reader
    odata_cl = lz4_reader_cl(idata, ilen, verbose=verbose)
    offset = 0
    lz77_instr_lst = []
    while(True):
        r, oshift = LZ77_InDataT_unpack(odata_cl, offset=offset)
        offset += oshift
        lz77_instr_lst.append(r)
        if r['flag']:
            break
    if verbose:
        print(lz77_instr_lst)
    for i, lz77_instr in enumerate(lz77_instr_lst):
        if lz77_instr['is_literal']==0:
            if verbose:
                print(i)
                print(lz77_instr_lst[i-1], lz77_instr_lst[i], lz77_instr_lst[i+1], lz77_instr_lst[i+2])
                break
    decompressed, _, flag = lz77_decode_instructions(lz77_instr_lst, verbose=False)
    return decompressed, flag

def do_single_test(idata, ilen, ref, verbose=True):
    if verbose:
        print(idata)
        print('size:', idata.size)
    decompressed, flag = lz4_reader_run(idata, ilen, verbose)
    if len(decompressed)<255:
        print(decompressed.decode('utf-8'))
    if verbose:
        print(flag)
        print('cl decompressed bytes: %d, ref. bytes: %d' % (len(decompressed), ref.size,))
    if(len(decompressed)<ref.size):
        test_passed = False
    else:
        test_passed = np.all(np.frombuffer(decompressed, np.uint8)[:ref.size]==ref)
    if verbose:
        print('Test passed:', test_passed)
    if verbose and not test_passed:
        min_sz = len(decompressed) if len(decompressed)<ref.size else ref.size 
        print(np.argwhere(np.frombuffer(decompressed, np.uint8)[:min_sz]!=ref[:min_sz])[0][0])
        #print(np.frombuffer(decompressed, np.uint8)[250:290])
        #print(ref[250:290])
    # find the last nonzero output byte
    #idx_last = np.argwhere(odata_cl)[-1][-1] + 1
    #print(odata_cl[:idx_last])
    #print(odata_cl[:50])
    return test_passed
    
if __name__ == "__main__":
    if test_id==1:
        idata, ilen, ref, _ = LZ4_test1(3921) # 8857
        test_passed = do_single_test(idata, ilen, ref, verbose=True)
    elif test_id==-1:
        ipassed = 0
        itested = 0
        for iblk in range(20000):
            try:
                idata, ilen, ref, rlen = LZ4_test1(iblk)
                test_passed = do_single_test(idata, ilen, ref, verbose=False)
                if not test_passed:
                    print('failed iblk:', iblk)
                ipassed = ipassed+1 if test_passed else ipassed
                itested += 1
            except IndexError:
                print('Test reached end of test-file.')
                break
            except Exception as e:
                print(e)
            if ((iblk+1) % 500 == 0):
                print('... finished %d blocks' % (iblk+1)) 
        print('Test passed for %d from %d tested blocks.' % (ipassed,itested,))
        print('Remaining %d bytes' % rlen)
        print('Test passed:', itested==ipassed and itested>0)
    elif test_id==2:
        idata, ilen, ref = LZ4_test2()
        test_passed = do_single_test(idata, ilen, ref, verbose=True)
    elif test_id==3:
        idata, ilen, ref = LZ4_test3()
        test_passed = do_single_test(idata, ilen, ref, verbose=True)        
    else:
        idata, ilen, ref = LZ4_test0()
        test_passed = do_single_test(idata, ilen, ref, verbose=True)

# ------------------------------------------------------------------------
# Usage: CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1 PYOPENCL_CTX='1' python lz4_app.py -d -c fpga -t 0

